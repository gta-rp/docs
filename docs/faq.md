# Preguntas frequentes
- - -
## ¿Qué es TranscendRP?
> A magical documentation site generator.
## ¿Puedo ser parte del staff?
> Por supuesto que si, pero tenemos unos requisitos concretos para entrar en la selección.   
Nunca aseguramos que puedas totalmente entrar.  Tienes que tener minímo 18 años por temas legales.  
Tenemos más información al respecto por si te interesa revisarlo. [Click aquí](http://docs.trascend-rp.com/#/howto-staff)  
## He sido testigo de abuso de poder por parte del staff, que hago?
> Para comenzar, posiblemente no era un admin, si no un hacker, si fuese el caso repotalo en el discord, y intenta comunicarle algíun administrador lo sucedido.  
Solo hay dos personas con poderes ingame, **Danitrebu** y **Nirzhuk**, dueños del servidor.  
Este servidor incitamos que todo pase por la policia, excepto los hackers/trols/personas que incumplen muy desmesurada las normas. Ellos son la ley, no el staff.  
Por ende, nunca tendrás la capacidad de hablar con un admin ingame, solo por discord.  

## He recibido un *kick* o un *ban* sin motivo, Por qué?
> Normalmente esto lo hace **Nirzhuk** cuando comprueba logs del servidor y ve que has incumplido normas, uso exploits, hacks, etc.  
Es un gran hermano, pensarlo de esa manera.  


### *Esta web se actualizará cuando veamos que hay problemas que necesitan respuestas comunes*