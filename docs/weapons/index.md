# Reglas con armas 

- Para hacer uso de las armas, SIEMPRE debe haber un rol previo
- El francotirador sólo podrá ser usado desde zonas estáticas
    Por ejemplo, desde una azotea mientras se está produciendo una negociación con rehenes.