# Reglas para el cuerpo de policía

### Los policías ÚNICAMENTE podrán usar las armas designadas para su trabajo
Éstas son, las obtenidas en comisaría
### NUNCA podrán realizar acciones ilegales, dentro o fuera de servicio
### Corrupción
Debido que un policia corrupto tiene un poder casi ilimitado, este rol no esta permitido.
Si en algún caso algún policia hiciese esa linea argumental, se le distuiría y se le empresionaria durante 1 semana.

### La policía NUNCA hará un uso desproporcionado de la fuerza

### La policía podrá cachear cuando tenga una sospecha o aviso sobre la misma persona, NUNCA sin razón

### Sólo abrir fuego contra otro vehículo siempre que el mismo haya sido avisado varias veces de parar/rendirse

---

## Negociaciones

### Con atracadores sin rehénes
- Comprobación de la situación

### Con atracadores con **rehénes**

Podrán ser quebrantadas si han pasado más de 20 minutos, para que el rol pueda continuar.