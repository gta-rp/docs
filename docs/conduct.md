
# Normas básicas de comportamiento

<ul>
    <li><b>Respeto</b></li>
    Se deben respetar a todos los jugadores por igual. Se podrá hacer un uso moderado del lenguaje ofensivo siempre y cuando el rol lo requiera, y NUNCA hacia la persona que interpreta el personaje.
    <li><b>No acaparar el chat de voz</b></li>
    Facilitando así el escuchar correctamente a los demás. Se deberá respetar el turno de palabra en la medida de lo posible.
    <li><b>No hacer stream sniping</b></li>
    Queda terminantemente prohibido ver retransmisiones en directo de los demás jugadores sólo para ir allá donde están ellos e interacturar. El flujo de interacción debe de ser algo natural y no forzado.
    <li><b>No usar los canales de forma incorrecta</b></li>
    Habrá que usar los diferentes canales designados para cada acción.
    <li><b>No hacer bunny jump</b></li>
    No está permitido saltar de forma continuada para correr/huir así más rápido, para esquivar balas enemigas o cualquier otro tipo de situación.
    <li><b>No contactar con los administradores de forma personal</b></li>
    Los administradores que estén en línea ya estarán haciendo su trabajo, observando o hablando con otros jugadores. Para contactar con ellos, se deberá de usar el canal designado por el servidor.
</ul>
