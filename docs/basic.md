# Documentación

### Respeto
Se deben respetar a todos los jugadores por igual. Se podrá hacer un uso moderado del lenguaje ofensivo siempre y cuando el rol lo requiera, y NUNCA hacia la persona que interpreta el personaje.
### No acaparar el chat de voz
Facilitando así el escuchar correctamente a los demás. Se deberá respetar el turno de palabra en la medida de lo posible.

### No hacer stream sniping
Queda terminantemente prohibido ver retransmisiones en directo de los demás jugadores sólo para ir allá donde están ellos e interacturar. El flujo de interacción debe de ser algo natural y no forzado.

### No usar los canales de forma incorrecta
Habrá que usar los diferentes canales designados para cada acción.

### No hacer bunny jump
No está permitido saltar de forma continuada para correr/huir así más rápido, para esquivar balas enemigas o cualquier otro tipo de situación.

### No contactar con los administradores de forma personal
Los administradores que estén en línea ya estarán haciendo su trabajo, observando o hablando con otros jugadores. Para contactar con ellos, se deberá de usar el canal designado por el servidor.



# Reglas de rol básicas

### Valorar la vida del personaje
### NUNCA evitar situaciones de rol
No está permitido ignorar el rol de los demás (por ejemplo, haciendo como que no le estás escuchando por problemas en tus auriculares o simulando estar ausente)
### SIEMPRE interpretar el personaje y seguir su rol
No se podrá hablar fuera del personaje, únicamente para dudas y por el canal destinado a ello (<kbd>/ooc</kbd>)
### NUNCA realizar llamadas falsas a servicios de emergencia/policía
Son cuerpos con un menor número de jugadores que civiles, y deben de estar siempre disponibles para realizar sus funciones y continuar su rol.
### Al morir, se perderá todo recuerdo que se tenga de lo sucedido
No se podrá volver al lugar de la muerte para continuar con determinado rol, o buscar al jugador con el que estuviéramos para matarlo o cualquier tipo de acción.
### No hacer revenge kill (RK)
Derivada de la norma anterior, al morir, no está permitido volver al lugar donde se murió o buscar al jugador por el que fuiste asesinado para vengarte.
### No hacer meta gaming (MG)
Esto es, obtener información fuera del personaje, como por ejemplo en una retransmisión en directo, para usarla dentro del rol en su beneficio.
No hacer power gaming (PG)
Esto es, realizar acciones irrealizables en la vida real (por ejemplo, rolear que saltas 20 metros) o forzar el rol de los demás, sin darles opción a nada.
### No hacer deathmatch (DM)
No se puede matar por matar en el servidor. Toda situación de riesgo debe ser roleada anteriormente.

