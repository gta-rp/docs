# Normas y documentación de TrascendRP
















## Como participar en las normas de la comunidad

1 - Descarga el proyecto en tu local. Si sabes utilizar Git, clonalo como un proyecto más. Si no sabes, te dejo un vídeo para que tengas una noción rápida: https://www.youtube.com/watch?v=3XlZWpLwvvo
!['clone'](./images/clone.png)
2 - Haces las modificaciones que necesites en local.

3 - Creas una nueva branch con `git checkout -b NOMBRE_DE_LA_BRANCH`, utilizamos una anglomeratura para las sugerencias de normas y documentación así que sería bueno que el nombre de la branch fuera así: `normas/nombre_general_de_tu_modificacion`. Ejemplo `normas/fix_ortografia_policia`

4 - Después al crearla y "pushear" los cambios( el video puesto,te explcia todo ) iriamos de nuevo a gitlab y creariamos una **Merge Request**.
!['merge'](./images/merge.png)
- Seleccionariamos la nueva branch y esperariamos que nos revisarán los cambios.
Necesitaria que Danitrebu o Nirzhuk te dieran el OKEY para que ellos añadieran el cambio.